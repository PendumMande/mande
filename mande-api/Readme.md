# Mande API

This folder contains all the files related to the API configuration and logic about of the Mande BE.

## Installation

At first, you need to create a .env file where all the config is located, it looks like this:

```bash
DB_NAME =
DB_USER =
DB_PASS =
DB_PORT =
DB_HOST = 
DIALECT_DB_CONFIG = postgres
PROTOCOL_DB_CONFIG = postgres
PORT    = 8084
ONLINE  = 
SECRET_KEY = any_secret
```

Please, keep in mind that if you will use an online database set as true the ONLINE parameter. This env file is referred to a Postgres configuration if you use another DB please search into the sequelize documentation in order to verify how the configuration should look like.

Install all the needed dependencies:

```bash
npm install -f
```

if you are using a Unix environment please review that you have the needed permissions to add files, or run as sudo.

## Usage

### Development Environment

```bash
npm run dev
```

### Running the linter

```bash
npm run pretest
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.