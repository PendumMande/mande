import config from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import userRoutes from './server/routes/UserRoutes';
import apiRoutes from './server/routes/ApiRoutes';
import receiptRoutes from './server/routes/ReceiptRoutes';
import addressRoutes from './server/routes/AddressRoutes';
import paymentMethodRoutes from './server/routes/PaymentMethodRoutes';
import paymentMethodCreditRoutes from './server/routes/PaymentMethodCreditRoutes';
import paymentMethodDebitRoutes from './server/routes/PaymentMethodDebitRoutes';
import workRoutes from './server/routes/WorkRoutes';
import walletRoutes from './server/routes/WalletRoutes';
import clientWorkerRoutes from './server/routes/ClientWorkerRoutes';
import workerWorkRoutes from './server/routes/WorkerWorkRoutes';

config.config();

const app = express();

const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

const port = process.env.PORT || 8084;

app.use('/api/v1/user', userRoutes);
app.use('/api/v1/auth', apiRoutes);
app.use('/api/v1/receipt', receiptRoutes);
app.use('/api/v1/address', addressRoutes);
app.use('/api/v1/paymentMethod', paymentMethodRoutes);
app.use('/api/v1/paymentMethodCredit', paymentMethodCreditRoutes);
app.use('/api/v1/paymentMethodDebit', paymentMethodDebitRoutes);
app.use('/api/v1/work', workRoutes)
app.use('/api/v1/wallet', walletRoutes);
app.use('/api/v1/clientWorker', clientWorkerRoutes);
app.use('/api/v1/workerWork', workerWorkRoutes);

// when a random route is inputed
app.get('*', (req, res) => res.status(404).send({
  message: 'URL NOT FOUND',
}));

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});

export default app;
