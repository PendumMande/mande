//const express = require('express')
const NodeGeocoder = require('node-geocoder')
// dejar elegir los 3 resultados mas cercanos !!!
//const app = express()
//const port = 3000
const geocoder = NodeGeocoder({
    provider: 'opencage',
    apiKey: 'a48fcd9df26f4663bbf5e2c05593a3ac',
    limit: 1
})
class GeoCoder {

    static async getLatLong(address) {
        const direccion = address.direccion_direccion;
        const newDireccion = await geocoder.geocode(`${direccion} cali colombia`);
        const direccionReturn = JSON.stringify({ id_usuario: address.id_usuario, direccion_latitud: newDireccion[0].latitude, direccion_longitud: newDireccion[0].longitude, direccion_direccion: direccion });
        return direccionReturn;
    }
}



export default GeoCoder;