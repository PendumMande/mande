import WorkService from '../services/WorkService';
import Util from '../utils/Utils';

const util = new Util();

class WorksController {
    static async getAllWorks(req, res) {
        try {
            const allWorks = await WorkService.getAllWorks();
            if (allWorks.length > 0) {
                util.setSuccess(200, 'Works retrieved', allWorks);
            } else {
                util.setSuccess(500, 'No works found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }

    static async addWork(req, res) {
        if (req.body.usuario_tipo === 1) {
            util.setError(400, 'You do not have permission to do this action');
            return util.send(res);
        }
        const newWork = req.body;
        try {
            const createdWork = await WorkService.addWork(newWork);
            util.setSuccess(201, 'Work Added!', createdWork);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    }

    static async updateWorkById(req, res) {
        const alteredWork = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateWork = await WorkService.updateWorkById(id, alteredWork);
            if (!updateWork) {
                util.setError(404, `Cannot find work with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Work updated', updateWork);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async getWorkById(req, res) {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theWork = await WorkService.getWorkById(id);

            if (!theWork) {
                util.setError(404, `Cannot find work with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found work', theWork);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async deleteWorkById(req, res) {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }

        try {
            const workToDelete = await WorkService.deleteWorkById(id);

            if (workToDelete) {
                util.setSuccess(200, 'Work deleted');
            } else {
                util.setError(404, `Work with the id ${id} cannot be found`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }
}

export default WorksController;
