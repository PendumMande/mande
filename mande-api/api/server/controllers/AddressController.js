import AddressService from '../services/AddressService';
import GeoCoder from '../Geocoder';
import Util from '../utils/Utils';

const util = new Util();

class AddressController {

    static async getAllAddresses(req, res) {
        try {
            const allAddress = await AddressService.getAllAddresses();
            if (allAddress.length > 0) {
                util.setSuccess(200, 'Address retrieved', allAddress);
            } else {
                util.setSuccess(500, 'No Address found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }

    static async addAddress(req, res) {
        const newAddress = await GeoCoder.getLatLong(req.body);
        try {
            const createdAddress = await AddressService.addAddress(JSON.parse(newAddress));
            util.setSuccess(201, 'Address Added!', createdAddress);
            return util.send(res);
        } catch (error) {
            console.log(error);
            util.setError(400, error.message);
            return util.send(res);
        }
    }

    static async updateAddressById(req, res) {
        const alteredAddress = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateAddress = await AddressService.updateAddressById(id, alteredAddress);
            if (!updateAddress) {
                util.setError(404, `Cannot find Address with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Address updated', updateAddress);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async getAddressById(req, res) {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theAddress = await AddressService.getAddressById(id);

            if (!theAddress) {
                util.setError(404, `Cannot find Address with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found Address', theAddress);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async getUserById(req, res) {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theAddress = await AddressService.getUserById(id);

            if (!theAddress) {
                util.setError(404, `Cannot find Address with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found Address', theAddress);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }

    static async deleteAddressById(req, res) {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }

        try {
            const addressToDelete = await AddressService.deleteAddressById(id);

            if (addressToDelete) {
                util.setSuccess(200, 'Address deleted');
            } else {
                util.setError(404, `Address with the id ${id} cannot be found`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }
}

export default AddressController;
