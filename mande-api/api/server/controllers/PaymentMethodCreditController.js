import PaymentMethodCreditService from '../services/PaymentMethodCreditService';
import Util from '../utils/Utils';

const util = new Util();

class PaymentMethodCreditController {
  static async getAllPaymentMethodCredits(req, res) {
    try {
      const allPaymentMethodCredits = await PaymentMethodCreditService.getAllPaymentMethodCredits();
      if (allPaymentMethodCredits.length > 0) {
        util.setSuccess(200, ' Credit wallets retrieved', allPaymentMethodCredits);
      } else {
        util.setSuccess(500, 'No credit wallets found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addPaymentMethodCredit(req, res) {
    const newPaymentMethodCredit = req.body;
    try {
      const createdPaymentMethodCredit = await PaymentMethodCreditService.addPaymentMethodCredit(newPaymentMethodCredit);
      util.setSuccess(201, 'Credit wallet Added!', createdPaymentMethodCredit);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updatePaymentMethodCreditById(req, res) {
    const alteredPaymentMethodCredit = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updatePaymentMethodCredit = await PaymentMethodCreditService.updatePaymentMethodCreditById(id, alteredPaymentMethodCredit);
      if (!updatePaymentMethodCredit) {
        util.setError(404, `Cannot find credit wallet with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Credit wallet updated', updatePaymentMethodCredit);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getPaymentMethodCreditById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const thePaymentMethodCredit = await PaymentMethodCreditService.getPaymentMethodCreditById(id);

      if (!thePaymentMethodCredit) {
        util.setError(404, `Cannot find credit wallet with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found credit wallet', thePaymentMethodCredit);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deletePaymentMethodCreditById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const paymentMethodCreditToDelete = await PaymentMethodCreditService.deletePaymentMethodCreditById(id);

      if (paymentMethodCreditToDelete) {
        util.setSuccess(200, 'Credit wallet deleted');
      } else {
        util.setError(404, `Credit wallet with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default PaymentMethodCreditController;
