import PaymentMethodDebitService from '../services/PaymentMethodDebitService';
import Util from '../utils/Utils';

const util = new Util();

class PaymentMethodDebitController {
  static async getAllPaymentMethodDebits(req, res) {
    try {
      const allPaymentMethodDebits = await PaymentMethodDebitService.getAllPaymentMethodDebits();
      if (allPaymentMethodDebits.length > 0) {
        util.setSuccess(200, 'Debit wallets retrieved', allPaymentMethodDebits);
      } else {
        util.setSuccess(500, 'No debit wallets found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addPaymentMethodDebit(req, res) {
    const newPaymentMethodDebit = req.body;
    try {
      const createdPaymentMethodDebit = await PaymentMethodDebitService.addPaymentMethodDebit(newPaymentMethodDebit);
      util.setSuccess(201, 'Debit wallet Added!', createdPaymentMethodDebit);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updatePaymentMethodDebitById(req, res) {
    const alteredPaymentMethodDebit = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updatePaymentMethodDebit = await PaymentMethodDebitService.updatePaymentMethodDebitById(id, alteredPaymentMethodDebit);
      if (!updatePaymentMethodDebit) {
        util.setError(404, `Debit wallet with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Debit wallet updated', updatePaymentMethodDebit);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getPaymentMethodDebitById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const thePaymentMethodDebit = await PaymentMethodDebitService.getPaymentMethodDebitById(id);

      if (!thePaymentMethodDebit) {
        util.setError(404, `Cannot find debit wallet with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found debit wallet', thePaymentMethodDebit);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deletePaymentMethodDebitById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const paymentMethodDebitToDelete = await PaymentMethodDebitService.deletePaymentMethodDebitById(id);

      if (paymentMethodDebitToDelete) {
        util.setSuccess(200, 'Debit wallet deleted');
      } else {
        util.setError(404, `Debit wallet with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default PaymentMethodDebitController;
