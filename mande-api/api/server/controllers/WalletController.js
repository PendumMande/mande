import WalletService from '../services/WalletService';
import Util from '../utils/Utils';

const util = new Util();

class WalletController {
  static async getAllWallets(req, res) {
    try {
      const allWallets = await WalletService.getAllWallets();
      if (allWallets.length > 0) {
        util.setSuccess(200, 'Wallets retrieved', allWallets);
      } else {
        util.setSuccess(500, 'No Wallets found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addWallet(req, res) {
    const newWallet = req.body;
    try {
      const createdWallet = await WalletService.addWallet(newWallet);
      util.setSuccess(201, 'Wallet Added!', createdWallet);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updateWalletById(req, res) {
    const alteredWallet = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateWallet = await WalletService.updateWalletById(id, alteredWallet);
      if (!updateWallet) {
        util.setError(404, `Cannot find wallet with the id: ${id}`);
      } else {
        util.setSuccess(200, 'wallet updated', updateWallet);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getWalletById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theWallet = await WalletService.getWalletById(id);

      if (!theWallet) {
        util.setError(404, `Cannot find wallet with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found wallet', theWallet);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deleteWalletById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const walletToDelete = await WalletService.deleteWalletById(id);

      if (walletToDelete) {
        util.setSuccess(200, 'Wallet deleted');
      } else {
        util.setError(404, `Wallet with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default WalletController;
