import PaymentMethodService from '../services/PaymentMethodService';
import Util from '../utils/Utils';

const util = new Util();

class PaymentMethodController {
  static async getAllPaymentMethods(req, res) {
    try {
      const allPaymentMethods = await PaymentMethodService.getAllPaymentMethods();
      if (allPaymentMethods.length > 0) {
        util.setSuccess(200, 'Wallets retrieved', allPaymentMethods);
      } else {
        util.setSuccess(500, 'No wallets found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addPaymentMethod(req, res) {
    const newPaymentMethod = req.body;
    try {
      const createdPaymentMethod = await PaymentMethodService.addPaymentMethod(newPaymentMethod);
      util.setSuccess(201, 'Wallet Added!', createdPaymentMethod);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updatePaymentMethodById(req, res) {
    const alteredPaymentMethod = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updatePaymentMethod = await PaymentMethodService.updatePaymentMethodById(id, alteredPaymentMethod);
      if (!updatePaymentMethod) {
        util.setError(404, `Cannot find PaymentMethod with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Wallet updated', updatePaymentMethod);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getPaymentMethodById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const thePaymentMethod = await PaymentMethodService.getPaymentMethodById(id);

      if (!thePaymentMethod) {
        util.setError(404, `Cannot find wallet with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found wallet', thePaymentMethod);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deletePaymentMethodById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const paymentMethodToDelete = await PaymentMethodService.deletePaymentMethodById(id);

      if (paymentMethodToDelete) {
        util.setSuccess(200, 'Wallet deleted');
      } else {
        util.setError(404, `Wallet with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default PaymentMethodController;
