import ClientWorkerService from '../services/ClientWorkerService';
import Util from '../utils/Utils';

const util = new Util();

class ClientWorkerController {
  static async getAllClientWorkers(req, res) {
    try {
      const allClientWorkers = await ClientWorkerService.getAllClientWorkers();
      if (allClientWorkers.length > 0) {
        util.setSuccess(200, 'ClientWorkers retrieved', allClientWorkers);
      } else {
        util.setSuccess(500, 'No ClientWorkers found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addClientWorker(req, res) {
    const newClientWorker = req.body;
    try {
      const createdClientWorker = await ClientWorkerService.addClientWorker(newClientWorker);
      util.setSuccess(201, 'ClientWorker Added!', createdClientWorker);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updateClientWorkerById(req, res) {
    const alteredClientWorker = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateClientWorker = await ClientWorkerService.updateClientWorkerById(id, alteredClientWorker);
      if (!updateClientWorker) {
        util.setError(404, `Cannot find ClientWorker with the id: ${id}`);
      } else {
        util.setSuccess(200, 'ClientWorker updated', updateClientWorker);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getClientWorkerById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theClientWorker = await ClientWorkerService.getClientWorkerById(id);

      if (!theClientWorker) {
        util.setError(404, `Cannot find ClientWorker with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found ClientWorker', theClientWorker);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deleteClientWorkerById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const clientWorkerToDelete = await ClientWorkerService.deleteClientWorkerById(id);

      if (clientWorkerToDelete) {
        util.setSuccess(200, 'ClientWorker deleted');
      } else {
        util.setError(404, `ClientWorker with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default ClientWorkerController;
