import WorkerWorkService from '../services/WorkerWorkService';
import Util from '../utils/Utils';

const util = new Util();

class WorkerWorkController {
  static async getAllWorkerWorks(req, res) {
    try {
      const allWorkerWorks = await WorkerWorkService.getAllWorkerWorks();
      if (allWorkerWorks.length > 0) {
        util.setSuccess(200, 'WorkerWorks retrieved', allWorkerWorks);
      } else {
        util.setSuccess(500, 'No WorkerWorks found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addWorkerWork(req, res) {

    const newWorkerWork = req.body;
    try {
      const createdWorkerWork = await WorkerWorkService.addWorkerWork(newWorkerWork);
      util.setSuccess(201, 'WorkerWork Added!', createdWorkerWork);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updateWorkerWorkById(req, res) {
    const alteredWorkerWork = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateWorkerWork = await WorkerWorkService.updateWorkerWorkById(id, alteredWorkerWork);
      if (!updateWorkerWork) {
        util.setError(404, `Cannot find WorkerWork with the id: ${id}`);
      } else {
        util.setSuccess(200, 'WorkerWork updated', updateWorkerWork);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getWorkerWorkById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theWorkerWork = await WorkerWorkService.getWorkerWorkById(id);

      if (!theWorkerWork) {
        util.setError(404, `Cannot find WorkerWork with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found WorkerWork', theWorkerWork);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getWorkerWorkUserById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theWorkerWork = await WorkerWorkService.getWorkerWorkUserById(id);

      if (!theWorkerWork) {
        util.setError(404, `Cannot find WorkerWork with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found WorkerWork', theWorkerWork);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getWorkerWorkReceiptById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theWorkerWork = await WorkerWorkService.getWorkerWorkReceiptById(id);

      if (!theWorkerWork) {
        util.setError(404, `Cannot find WorkerWork with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found WorkerWork', theWorkerWork);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deleteWorkerWorkById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const workerWorkToDelete = await WorkerWorkService.deleteWorkerWorkById(id);
      if (workerWorkToDelete) {
        util.setSuccess(200, 'WorkerWork deleted');
      } else {
        util.setError(404, `WorkerWork with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default WorkerWorkController;
