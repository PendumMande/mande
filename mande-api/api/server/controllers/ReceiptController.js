import ReceiptService from '../services/ReceiptService';
import Util from '../utils/Utils';

const util = new Util();

class ReceiptController {
  static async getAllReceipts(req, res) {
    try {
      const allReceipts = await ReceiptService.getAllReceipts();
      if (allReceipts.length > 0) {
        util.setSuccess(200, 'Receipts retrieved', allReceipts);
      } else {
        util.setSuccess(500, 'No Receipts found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async addReceipt(req, res) {
    const newReceipt = req.body;
    try {
      const createdReceipt = await ReceiptService.addReceipt(newReceipt);
      util.setSuccess(201, 'Receipt Added!', createdReceipt);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async updateReceiptById(req, res) {
    const alteredReceipt = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    try {
      const updateReceipt = await ReceiptService.updateReceiptById(id, alteredReceipt);
      if (!updateReceipt) {
        util.setError(404, `Cannot find Receipt with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Receipt updated', updateReceipt);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async getReceiptById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try {
      const theReceipt = await ReceiptService.getReceiptById(id);

      if (!theReceipt) {
        util.setError(404, `Cannot find Receipt with the id ${id}`);
      } else {
        util.setSuccess(200, 'Found Receipt', theReceipt);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  static async deleteReceiptById(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(400, 'Please provide a numeric value');
      return util.send(res);
    }

    try {
      const ReceiptToDelete = await ReceiptService.deleteReceiptById(id);

      if (ReceiptToDelete) {
        util.setSuccess(200, 'Receipt deleted');
      } else {
        util.setError(404, `Receipt with the id ${id} cannot be found`);
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }
}

export default ReceiptController;
