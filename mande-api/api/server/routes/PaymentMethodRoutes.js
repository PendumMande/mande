import { Router } from 'express';
import PaymentMethodController from '../controllers/PaymentMethodController.js';
const router = Router();

router.get('/', PaymentMethodController.getAllPaymentMethods);
router.post('/', PaymentMethodController.addPaymentMethod);
router.get('/:id', PaymentMethodController.getPaymentMethodById);
router.put('/:id', PaymentMethodController.updatePaymentMethodById);
router.delete('/:id', PaymentMethodController.deletePaymentMethodById);

export default router;