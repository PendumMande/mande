import { Router } from 'express';
import ReceiptController from '../controllers/ReceiptController.js';
const router = Router();

router.get('/', ReceiptController.getAllReceipts);
router.post('/', ReceiptController.addReceipt);
router.get('/:id', ReceiptController.getReceiptById);
router.put('/:id', ReceiptController.updateReceiptById);
router.delete('/:id', ReceiptController.deleteReceiptById);

export default router;