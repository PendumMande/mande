import { Router } from "express";

const router = Router();

router.get("/home", function (req, res) {
  res.send("Welcome!");
});

router.get("/secret", function (req, res) {
  res.send("The password is potato");
});

export default router;
