import { Router } from 'express';
import PaymentMethodCreditController from '../controllers/PaymentMethodCreditController.js';
const router = Router();

router.get('/', PaymentMethodCreditController.getAllPaymentMethodCredits);
router.post('/', PaymentMethodCreditController.addPaymentMethodCredit);
router.get('/:id', PaymentMethodCreditController.getPaymentMethodCreditById);
router.put('/:id', PaymentMethodCreditController.updatePaymentMethodCreditById);
router.delete('/:id', PaymentMethodCreditController.deletePaymentMethodCreditById);

export default router;