import { Router } from 'express';
import UserController from '../controllers/UserController';
const router = Router();

router.get('/', UserController.getAllUsers);
router.get('/address/:id', UserController.getUserWithAddressById);
router.get('/work/:id', UserController.getUserWorkerById);
router.post('/', UserController.addUser);
router.get('/:id', UserController.getUserById);
router.put('/:id', UserController.updateUserById);
router.delete('/:id', UserController.deleteUserById);
router.post('/login', UserController.doLogin);

export default router;