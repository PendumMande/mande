import { Router } from 'express';
import WorksController from '../controllers/WorksController';
const router = Router();

router.get("/working", function (req, res) {
    res.send("working!");
  });
router.get('/', WorksController.getAllWorks);
router.post('/', WorksController.addWork);
router.get('/:id', WorksController.getWorkById);
router.put('/:id', WorksController.updateWorkById);
router.delete('/:id', WorksController.deleteWorkById);

export default router;