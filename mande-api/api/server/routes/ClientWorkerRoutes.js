import { Router } from 'express';
import ClientWorkerController from '../controllers/ClientWorkerController';
const router = Router();

router.get('/', ClientWorkerController.getAllClientWorkers);
router.post('/', ClientWorkerController.addClientWorker);
router.get('/:id', ClientWorkerController.getClientWorkerById);
router.put('/:id', ClientWorkerController.updateClientWorkerById);
router.delete('/:id', ClientWorkerController.deleteClientWorkerById);

export default router;