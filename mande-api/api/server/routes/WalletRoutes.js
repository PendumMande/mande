import { Router } from 'express';
import WalletController from '../controllers/WalletController';
const router = Router();

router.get('/', WalletController.getAllWallets);
router.post('/', WalletController.addWallet);
router.get('/:id', WalletController.getWalletById);
router.put('/:id', WalletController.updateWalletById);
router.delete('/:id', WalletController.deleteWalletById);

export default router;