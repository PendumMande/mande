import { Router } from 'express';
import WorkerWorkController from '../controllers/WorkerWorkController';
const router = Router();

router.get('/', WorkerWorkController.getAllWorkerWorks);
router.post('/', WorkerWorkController.addWorkerWork);
router.get('/:id', WorkerWorkController.getWorkerWorkById);
//retorna los trabajadores con toda su informacion, filtrados por el id_labor
router.get('/user/:id', WorkerWorkController.getWorkerWorkUserById);
//retorna todos los recibos pendientes filtrados por el id_usuario del trabajador
router.get('/receipt/:id', WorkerWorkController.getWorkerWorkReceiptById);
router.put('/:id', WorkerWorkController.updateWorkerWorkById);
router.delete('/:id', WorkerWorkController.deleteWorkerWorkById);

export default router;