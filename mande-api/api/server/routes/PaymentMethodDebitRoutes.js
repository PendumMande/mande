import { Router } from 'express';
import PaymentMethodDebitController from '../controllers/PaymentMethodDebitController.js';
const router = Router();

router.get('/', PaymentMethodDebitController.getAllPaymentMethodDebits);
router.post('/', PaymentMethodDebitController.addPaymentMethodDebit);
router.get('/:id', PaymentMethodDebitController.getPaymentMethodDebitById);
router.put('/:id', PaymentMethodDebitController.updatePaymentMethodDebitById);
router.delete('/:id', PaymentMethodDebitController.deletePaymentMethodDebitById);

export default router;