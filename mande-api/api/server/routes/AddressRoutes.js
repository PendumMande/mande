import { Router } from 'express';
import AddressController from '../controllers/AddressController.js';
const router = Router();

router.get('/', AddressController.getAllAddresses);
router.post('/', AddressController.addAddress);
router.get('/:id', AddressController.getAddressById);
router.get('/user/:id', AddressController.getUserById);
router.put('/:id', AddressController.updateAddressById);
router.delete('/:id', AddressController.deleteAddressById);

export default router;