module.exports = (sequelize, DataTypes) => {
  const usuario_trabajador = sequelize.define('usuario_trabajador', {
    id_usuario: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    trabajador_puntuacion: {
      type: DataTypes.INTEGER,
      allowNull: false,
      isNumeric: true
    },
    trabajador_foto_cedula: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    trabajador_ocupado: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {
    indexes: [
      {
        name: 'usuario_id_trabajador_index',
        unique: true,
        fields: ['id_usuario']
      }
    ],
    freezeTableName: true,
  });
  usuario_trabajador.associate = function (models) {
    usuario_trabajador.hasMany(models.labor_trabajador, { foreignKey: 'id_usuario', as: 'trabajador_labor', onDelete: 'CASCADE' });
    usuario_trabajador.belongsTo(models.usuario, { foreignKey: 'id_usuario', as: 'usuario_trabajador', onDelete: 'CASCADE' });
  };
  usuario_trabajador.sync({ logging: true });
  return usuario_trabajador;
};