'use strict';
module.exports = (sequelize, DataTypes) => {
  const usuario = sequelize.define('usuario', {
    id_usuario: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    usuario_email: {
      type: DataTypes.STRING(64),
      allowNull: false,
      isEmail: true
    },
    usuario_celular: {
      type: DataTypes.BIGINT,
      allowNull: false,
      isNumeric: true
    },
    usuario_nombre: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    usuario_recibo_publico: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    usuario_contrasena: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    usuario_foto_perfil: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    usuario_tipo: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    indexes: [
      {
        name: 'usuario_email_usuario_index',
        unique: true,
        fields: ['usuario_email']
      },
      {
        name: 'usuario_celular_usuario_index',
        unique: true,
        fields: ['usuario_celular']
      },
      {
        name: 'usuario_id_usuario_index',
        unique: true,
        fields: ['id_usuario']
      }
    ],
    freezeTableName: true,
  });

  usuario.associate = function (models) {
    usuario.hasMany(models.recibo, { foreignKey: 'id_usuario_cliente', as: 'cliente_recibo', onDelete: 'CASCADE' });
    usuario.hasMany(models.medio_pago, { foreignKey: 'id_usuario', as: 'medio_pago', onDelete: 'CASCADE' });
    usuario.hasOne(models.monedera_usuario, { foreignKey: 'id_usuario', as: 'monedera_usuario', onDelete: 'CASCADE' });
    usuario.hasOne(models.direccion, { foreignKey: 'id_usuario', as: 'direccion_usuario', onDelete: 'CASCADE' });
    usuario.hasOne(models.usuario_trabajador, { foreignKey: 'id_usuario', as: 'usuario_trabajador', onDelete: 'CASCADE' });
  };
  usuario.sync({ logging: true });
  return usuario;
};