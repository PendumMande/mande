'use strict';
module.exports = (sequelize, DataTypes) => {
  const labor = sequelize.define('labor', {
    id_labor: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    labor_nombre: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    labor_imagen: {
      type: DataTypes.STRING(64),
      allowNull: false
    }
  }, {
    indexes: [
      {
        name: 'labor_id_labor_index',
        unique: true,
        fields: ['id_labor']
      },
      {
        name: 'labor_labor_nombre_index',
        unique: true,
        fields: ['labor_nombre']
      }
    ],
    freezeTableName: true,

  });
  labor.associate = function (models) {
    labor.hasMany(models.labor_trabajador, { foreignKey: 'id_labor', as: 'labor_trabajador_labor', onDelete: 'CASCADE' });
  };
  labor.sync({ logging: true });
  return labor;
};