'use strict';
module.exports = (sequelize, DataTypes) => {
  const labor_trabajador = sequelize.define('labor_trabajador', {
    id_labor_trabajador: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    id_labor: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    labor_precio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    labor_tipo: {
      type: DataTypes.STRING(12),
      allowNull: false
    }
  }, {
    indexes: [
      {
        name: 'labor_trabajador_id_labor_index',
        fields: ['id_labor']
      },
      {
        name: 'labor_trabajador_id_labor_trabajador_index',
        fields: ['id_labor_trabajador']
      },
      {
        name: 'labor_trabajador_id_trabajador_index',
        fields: ['id_usuario']
      },
      {
        name: 'labor_trabajador_labor_trabajador_precio_index',
        fields: ['labor_precio']
      },
      {
        name: 'labor_trabajador_labor_trabajador_tipo_index',
        fields: ['labor_tipo']
      }
    ],
    freezeTableName: true,
  });
  labor_trabajador.associate = function (models) {
    labor_trabajador.belongsTo(models.usuario_trabajador, { foreignKey: 'id_usuario', as: 'trabajador_labor', onDelete: 'CASCADE' })
    labor_trabajador.belongsTo(models.labor, { foreignKey: 'id_labor', as: 'labor_trabajador_labor', onDelete: 'CASCADE' });
    labor_trabajador.hasMany(models.recibo, { foreignKey: 'id_labor_trabajador', as: 'labor_trabajador_recibo_usuario', onDelete: 'CASCADE' })
  };
  labor_trabajador.sync({ logging: true });
  return labor_trabajador;
};