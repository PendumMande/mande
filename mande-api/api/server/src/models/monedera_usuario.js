module.exports = (sequelize, DataTypes) => {
  const monedera_usuario = sequelize.define('monedera_usuario', {
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false,
      isNumeric: true
    },
    monedera_cantidad: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
  }, {
    indexes: [
      {
        name: 'usuario_id_index',
        unique: true,
        fields: ['id_usuario']
      }
    ],
    freezeTableName: true,
  });
  monedera_usuario.associate = function (models) {
    monedera_usuario.belongsTo(models.usuario, { foreignKey: 'id_usuario', as: 'monedera_usuario', onDelete: 'CASCADE' });
  };
  monedera_usuario.sync({ logging: true });
  return monedera_usuario;
};
