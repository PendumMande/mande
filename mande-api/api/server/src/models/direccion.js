'use strict';
module.exports = (sequelize, DataTypes) => {
  const direccion = sequelize.define('direccion', {
    id_direccion: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    direccion_latitud: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    direccion_longitud: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    direccion_direccion: {
      type: DataTypes.STRING(64),
      allowNull: false
    }
  }, {
    indexes: [
      {
        name: 'direccion_id_direccion_index',
        fields: ['id_direccion']
      },
      {
        name: 'direccion_direccion_latitud_index',
        fields: ['direccion_latitud']
      },
      {
        name: 'direccion_direccion_longitud_index',
        fields: ['direccion_longitud']
      }
    ],
    freezeTableName: true,
  });
  direccion.associate = function (models) {
    direccion.belongsTo(models.usuario, { foreignKey: 'id_usuario', as: 'direccion_usuario', onDelete: 'CASCADE' });
  };
  direccion.sync({ logging: true });
  return direccion;
};