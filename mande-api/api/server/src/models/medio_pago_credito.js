module.exports = (sequelize, DataTypes) => {
    const medio_pago_credito = sequelize.define('medio_pago_credito', {
        id_medio_pago: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true
        },
        medio_pago_cvv: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true
        },
        medio_pago_nombre: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        medio_pago_expedicion: {
            type: DataTypes.DATE,
            allowNull: false
        },
        medio_pago_tarjeta: {
            type: DataTypes.BIGINT(16),
            allowNull: false,
            isNumeric: true
        },
    }, {
        freezeTableName: true,
    });
    medio_pago_credito.associate = function (models) {
        medio_pago_credito.belongsTo(models.medio_pago, { foreignKey: 'id_medio_pago', as: 'medio_pago', onDelete: 'CASCADE' });
    };
    medio_pago_credito.sync({ logging: true });
    return medio_pago_credito;
};