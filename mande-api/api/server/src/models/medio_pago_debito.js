module.exports = (sequelize, DataTypes) => {
    const medio_pago_debito = sequelize.define('medio_pago_debito', {
        id_medio_pago: {
            type: DataTypes.INTEGER,
            allowNull: false,
            isNumeric: true,
            unique: true
        },
        medio_pago_cuenta: {
            type: DataTypes.BIGINT(16),
            allowNull: false,
            isNumeric: true
        },
    }, {
        freezeTableName: true,
    });
    medio_pago_debito.associate = function (models) {
        medio_pago_debito.belongsTo(models.medio_pago, { foreignKey: 'id_medio_pago', as: 'medio_pago', onDelete: 'CASCADE' });
    };
    medio_pago_debito.sync({ logging: true });
    return medio_pago_debito;
};