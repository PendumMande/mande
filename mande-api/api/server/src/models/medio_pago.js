module.exports = (sequelize, DataTypes) => {
  const medio_pago = sequelize.define('medio_pago', {
    id_medio_pago: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    indexes: [
      {
        name: 'medio_pago_usuario_id_index',
        unique: true,
        fields: ['id_medio_pago']
      }
    ],
    freezeTableName: true,
  });
  medio_pago.associate = function (models) {
    medio_pago.hasOne(models.medio_pago_credito, { foreignKey: 'id_medio_pago', as: 'medio_pago_credito', onDelete: 'CASCADE' });
    medio_pago.hasOne(models.medio_pago_debito, { foreignKey: 'id_medio_pago', as: 'medio_pago_debito', onDelete: 'CASCADE' });
    medio_pago.belongsTo(models.usuario, { foreignKey: 'id_usuario', as: 'monedera_usuario', onDelete: 'CASCADE' });
  };
  medio_pago.sync({ logging: true });
  return medio_pago;
};