'use strict';
module.exports = (sequelize, DataTypes) => {
  const recibo = sequelize.define('recibo', {
    id_recibo: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    id_usuario_cliente: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_labor_trabajador: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    recibo_puntuacion: {
      type: DataTypes.INTEGER
    },
    recibo_estado: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    recibo_descripcion: {
      type: DataTypes.STRING(200)
    },
    recibo_pago: {
      type: DataTypes.DOUBLE
    },
    recibo_fecha_inicio: {
      type: DataTypes.DATE,
      allowNull: false
    },
    recibo_fecha_fin: {
      type: DataTypes.DATE
    },
  }, {
    indexes: [
      {
        name: 'recibo_usuario_id_index',
        fields: ['id_usuario_cliente']
      },
      {
        name: 'recibo_id_trabajador_index',
        fields: ['id_labor_trabajador']
      },
      {
        name: 'recibo_puntuacion_index',
        fields: ['recibo_puntuacion']
      }
    ],
    freezeTableName: true,

  });
  recibo.associate = function (models) {
    recibo.belongsTo(models.labor_trabajador, { foreignKey: 'id_labor_trabajador', as: 'labor_trabajador_recibo_usuario', onDelete: 'CASCADE' });
  };
  recibo.sync({ logging: true });
  return recibo;
};