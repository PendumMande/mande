'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('usuario', [
      {
      usuario_email: 'admin@mande.com',
      usuario_celular: 0,
      usuario_nombre: "admin",
      usuario_recibo_publico: "admin_recibo.png",
      usuario_contrasena: "admin123",
      usuario_foto_perfil: "admin.png",
      usuario_tipo: 0,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      usuario_email: 'trabajador@mande.com',
      usuario_celular: 1,
      usuario_nombre: "trabajador",
      usuario_recibo_publico: "trabajador_recibo.png",
      usuario_contrasena: "trabajador123",
      usuario_foto_perfil: "trabajador.png",
      usuario_tipo: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      usuario_email: 'cliente@mande.com',
      usuario_celular: 2,
      usuario_nombre: "cliente",
      usuario_recibo_publico: "cliente_recibo.png",
      usuario_contrasena: "cliente123",
      usuario_foto_perfil: "cliente.png",
      usuario_tipo: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('usuario', null, {});
  }
};
