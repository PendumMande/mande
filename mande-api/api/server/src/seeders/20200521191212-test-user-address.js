'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('usuario', [
      {
      id_usuario: 0,
      direccion_latitud: 3.3898293,
      direccion_longitud: -76.5343284,
      direccion_direccion: "Carrera 76 # 16 - 41",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('direccion', null, {});
  }
};
