'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('labor', [
      {
      labor_nombre: 'Pintor',
      labor_imagen: 'dyer.png',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      labor_nombre: 'Constructor',
      labor_imagen: 'builder.png',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      labor_nombre: 'Carpintero',
      labor_imagen: 'carpenter.png',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      labor_nombre: 'Chef',
      labor_imagen: 'chef.png',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      labor_nombre: 'Payaso',
      labor_imagen: 'clown.png',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      labor_nombre: 'Doctor',
      labor_imagen: 'doctor.png',
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('labor', null, {});
  }
};
