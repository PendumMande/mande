import database from '../src/models';

class AddressService {

    static async getAllAddresses() {
        try {
            return await database.direccion.findAll();
        } catch (error) {
            throw error;
        }
    }

    static async addAddress(newAddress) {
        try {
            return await database.direccion.create(newAddress);
        } catch (error) {
            throw error;
        }
    }

    static async updateAddressById(id, updateAddress) {
        try {
            const addressToUpdate = await database.direccion.findOne({
                where: { id_usuario: Number(id) }
            });

            if (addressToUpdate) {
                await database.direccion.update(updateAddress, { where: { id_usuario: Number(id) } });
                return updateAddress;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getAddressById(id) {
        try {
            const theAddress = await database.direccion.findOne({
                where: { id_usuario: Number(id) }
            });

            return theAddress;
        } catch (error) {
            throw error;
        }
    }

    static async getUserById(id) {
        try {
            const theUser = await database.direccion.findAll({
                include: [{
                    model: await database.usuario, required: false, as: "direccion_usuario",
                    include: [{
                        model: await database.usuario_trabajador, where: { trabajador_ocupado: false }, required: false, as: "usuario_trabajador",
                        include: [{
                            model: await database.labor_trabajador, required: false, as: "trabajador_labor"
                        }]
                    }]
                }], where: { id_usuario: Number(id) }
            });
            return theUser;
        } catch (error) {
            throw error;
        }
    }

    static async deleteAddressById(id) {
        try {
            const addressToDelete = await database.direccion.findOne({ where: { id_usuario: Number(id) } });

            if (addressToDelete) {
                const deletedAddress = await database.direccion.destroy({
                    where: { id_usuario: Number(id) }
                });
                return deletedAddress;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default AddressService;