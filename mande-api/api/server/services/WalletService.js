import database from '../src/models';

class WalletService {
  static async getAllWallets() {
    try {
      return await database.monedera_usuario.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addWallet(newWallet) {
    try {
      return await database.monedera_usuario.create(newWallet);
    } catch (error) {
      throw error;
    }
  }

  static async updateWalletById(id, updateWallet) {
    try {
      const walletToUpdate = await database.monedera_usuario.findOne({
        where: { id_usuario: Number(id) }
      });

      if (walletToUpdate) {
        await database.monedera_usuario.update(updateWallet, { where: { id_usuario: Number(id) } });

        return updateWallet;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getWalletById(id) {
    try {
      const theWallet = await database.monedera_usuario.findOne({
        where: { id_usuario: Number(id) }
      });

      return theWallet;
    } catch (error) {
      throw error;
    }
  }

  static async deleteWalletById(id) {
    try {
      const walletToDelete = await database.monedera_usuario.findOne({ where: { id_usuario: Number(id) } });

      if (walletToDelete) {
        const deletedWallet = await database.monedera_usuario.destroy({
          where: { id_usuario: Number(id) }
        });
        return deletedWallet;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default WalletService;