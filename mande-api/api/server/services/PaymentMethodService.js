import database from '../src/models';

class PaymentMethodService {
    static async getAllPaymentMethods() {
        try {
            return await database.medio_pago.findAll();
        } catch (error) {
            throw error;
        }
    }

    static async addPaymentMethod(newPaymentMethod) {
        try {
            return await database.medio_pago.create(newPaymentMethod);
        } catch (error) {
            throw error;
        }
    }

    static async updatePaymentMethodById(id, updatePaymentMethod) {
        try {
            const paymentMethodToUpdate = await database.medio_pago.findOne({
                where: { id_medio_pago: Number(id) }
            });

            if (paymentMethodToUpdate) {
                await database.medio_pago.update(updatePaymentMethod, { where: { id_medio_pago: Number(id) } });

                return updatePaymentMethod;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getPaymentMethodById(id) {
        try {
            const thePaymentMethod = await database.medio_pago.findOne({
                where: { id_medio_pago: Number(id) }
            });

            return thePaymentMethod;
        } catch (error) {
            throw error;
        }
    }

    static async deletePaymentMethodById(id) {
        try {
            const paymentMethodToDelete = await database.medio_pago.findOne({ where: { id_medio_pago: Number(id) } });

            if (paymentMethodToDelete) {
                const deletedPaymentMethod = await database.medio_pago.destroy({
                    where: { id_medio_pago: Number(id) }
                });
                return deletedPaymentMethod;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default PaymentMethodService;