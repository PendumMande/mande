import database from '../src/models';

class ClientWorkerService {
  static async getAllClientWorkers() {
    try {
      return await database.usuario_trabajador.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addClientWorker(newClientWorker) {
    try {
      return await database.usuario_trabajador.create(newClientWorker);
    } catch (error) {
      throw error;
    }
  }

  static async updateClientWorkerById(id, updateClientWorker) {
    try {
      const clientWorkerToUpdate = await database.usuario_trabajador.findOne({
        where: { id_usuario: Number(id) }
      });

      if (clientWorkerToUpdate) {
        await database.usuario_trabajador.update(updateClientWorker, { where: { id_usuario: Number(id) } });

        return updateClientWorker;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getClientWorkerById(id) {
    try {
      const theClientWorker = await database.usuario_trabajador.findOne({
        where: { id_usuario: Number(id) }
      });

      return theClientWorker;
    } catch (error) {
      throw error;
    }
  }

  static async deleteClientWorkerById(id) {
    try {
      const clientWorkerToDelete = await database.usuario_trabajador.findOne({ where: { id_usuario: Number(id) } });

      if (clientWorkerToDelete) {
        const deletedClientWorker = await database.usuario_trabajador.destroy({
          where: { id_usuario: Number(id) }
        });
        return deletedClientWorker;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default ClientWorkerService;