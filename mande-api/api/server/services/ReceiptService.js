import database from '../src/models';

class ReceiptService {
  static async getAllReceipts() {
    try {
      return await database.recibo.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addReceipt(newReceipt) {
    try {
      return await database.recibo.create(newReceipt);
    } catch (error) {
      throw error;
    }
  }

  static async updateReceiptById(id, updateReceipt) {
    try {
      const receiptToUpdate = await database.recibo.findOne({
        where: { id_recibo: Number(id) }
      });

      if (receiptToUpdate) {
        await database.recibo.update(updateReceipt, { where: { id_recibo: Number(id) } });

        return updateReceipt;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getReceiptById(id) {
    try {
      const theReceipt = await database.recibo.findOne({
        where: { id_recibo: Number(id) }
      });

      return theReceipt;
    } catch (error) {
      throw error;
    }
  }

  static async deleteReceiptById(id) {
    try {
      const receiptToDelete = await database.recibo.findOne({ where: { id_recibo: Number(id) } });

      if (receiptToDelete) {
        const deletedReceipt = await database.recibo.destroy({
          where: { id_recibo: Number(id) }
        });
        return deletedReceipt;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default ReceiptService;