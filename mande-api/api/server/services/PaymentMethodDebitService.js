import database from '../src/models';

class PaymentMethodDebitService {
    static async getAllPaymentMethodDebits() {
        try {
            return await database.medio_pago_debito.findAll();
        } catch (error) {
            throw error;
        }
    }

    static async addPaymentMethodDebit(newPaymentMethodDebit) {
        try {
            return await database.medio_pago_debito.create(newPaymentMethodDebit);
        } catch (error) {
            throw error;
        }
    }

    static async updatePaymentMethodDebitById(id, updatePaymentMethodDebit) {
        try {
            const paymentMethodDebitToUpdate = await database.medio_pago_debito.findOne({
                where: { id_medio_pago: Number(id) }
            });

            if (paymentMethodDebitToUpdate) {
                await database.medio_pago_debito.update(updatePaymentMethodDebit, { where: { id_medio_pago: Number(id) } });

                return updatePaymentMethodDebit;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getPaymentMethodDebitById(id) {
        try {
            const thePaymentMethodDebit = await database.medio_pago_debito.findOne({
                where: { id_medio_pago: Number(id) }
            });

            return thePaymentMethodDebit;
        } catch (error) {
            throw error;
        }
    }

    static async deletePaymentMethodDebitById(id) {
        try {
            const paymentMethodDebitToDelete = await database.medio_pago_debito.findOne({ where: { id_medio_pago: Number(id) } });

            if (paymentMethodDebitToDelete) {
                const deletedPaymentMethodDebit = await database.medio_pago_debito.destroy({
                    where: { id_medio_pago: Number(id) }
                });
                return deletedPaymentMethodDebit;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default PaymentMethodDebitService;