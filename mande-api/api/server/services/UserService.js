import database from '../src/models';

class UserService {
  static async getAllUsers() {
    try {
      return await database.usuario.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addUser(newUser) {
    try {
      return await database.usuario.create(newUser);
    } catch (error) {
      throw error;
    }
  }

  static async updateUserById(id, updateUser) {
    try {
      const userToUpdate = await database.usuario.findOne({
        where: { id_usuario: Number(id) }
      });

      if (userToUpdate) {
        await database.usuario.update(updateUser, { where: { id_usuario: Number(id) } });

        return updateUser;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getUserById(id) {
    try {
      const theUser = await database.usuario.findOne({
        where: { id_usuario: Number(id) }
      });

      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async getUserWithAddressById(id) {
    try {
      const theUser = await database.usuario.findOne({
        include: [{
          model: await database.direccion, required: true, as: "direccion_usuario"
        }], where: { id_usuario: Number(id) }
      });
      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async getUserWorkerById(id) {
    try {
      const theUser = await database.usuario.findAll({
        include: [{
          model: await database.usuario_trabajador, required: false, where: { trabajador_ocupado: false }, as: "usuario_trabajador",
          include: [{
            model: await database.labor_trabajador, required: false, as: "trabajador_labor"
          }]
        }], where: { id_usuario: Number(id) }
      });
      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async getUserByEmail(email, password) {
    try {
      const theUser = await database.usuario.findOne({
        attributes: { exclude: ['usuario_contrasena'] },
        include: [{
          model: await database.direccion, required: true, as: "direccion_usuario"
        }],
        where: { usuario_email: String(email), usuario_contrasena: String(password) }
      });

      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async getUserByMobile(celular, password) {
    try {
      const theUser = await database.usuario.findOne({
        attributes: { exclude: ['usuario_contrasena'] },
        include: [{
          model: await database.direccion, required: true, as: "direccion_usuario"
        }],
        where: { usuario_celular: Number(celular), usuario_contrasena: String(password) }
      });

      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async deleteUserById(id) {
    try {
      const userToDelete = await database.usuario.findOne({ where: { id_usuario: Number(id) } });

      if (userToDelete) {
        const deletedUser = await database.usuario.destroy({
          where: { id_usuario: Number(id) }
        });
        return deletedUser;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default UserService;