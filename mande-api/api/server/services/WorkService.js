import database from '../src/models';

class WorkService {
    static async getAllWorks() {
        try {
            return await database.labor.findAll();
        } catch (error) {
            throw error;
        }
    }

    static async addWork(newWork) {
        try {
            return await database.labor.create(newWork);
        } catch (error) {
            throw error;
        }
    }

    static async updateWorkById(id, updateWork) {
        try {
            const workToUpdate = await database.labor.findOne({
                where: { id_labor: Number(id) }
            });

            if (workToUpdate) {
                await database.labor.update(updateWork, { where: { id_labor: Number(id) } });

                return updateWork;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getWorkById(id) {
        try {
            const theWork = await database.labor.findOne({
                where: { id_labor: Number(id) }
            });

            return theWork;
        } catch (error) {
            throw error;
        }
    }

    static async deleteWorkById(id) {
        try {
            const workToDelete = await database.labor.findOne({ where: { id_labor: Number(id) } });

            if (workToDelete) {
                const deletedWork = await database.labor.destroy({
                    where: { id_labor: Number(id) }
                });
                return deletedWork;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default WorkService;