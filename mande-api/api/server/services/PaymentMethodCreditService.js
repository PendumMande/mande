import database from '../src/models';

class PaymentMethodCreditService {
    static async getAllPaymentMethodCredits() {
        try {
            return await database.medio_pago_credito.findAll();
        } catch (error) {
            throw error;
        }
    }

    static async addPaymentMethodCredit(newPaymentMethodCredit) {
        try {
            return await database.medio_pago_credito.create(newPaymentMethodCredit);
        } catch (error) {
            throw error;
        }
    }

    static async updatePaymentMethodCreditById(id, updatePaymentMethodCredit) {
        try {
            const paymentMethodCreditToUpdate = await database.medio_pago_credito.findOne({
                where: { id_medio_pago: Number(id) }
            });

            if (paymentMethodCreditToUpdate) {
                await database.medio_pago_credito.update(updatePaymentMethodCredit, { where: { id_medio_pago: Number(id) } });

                return updatePaymentMethodCredit;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }

    static async getPaymentMethodCreditById(id) {
        try {
            const thePaymentMethodCredit = await database.medio_pago_credito.findOne({
                where: { id_medio_pago: Number(id) }
            });

            return thePaymentMethodCredit;
        } catch (error) {
            throw error;
        }
    }

    static async deletePaymentMethodCreditById(id) {
        try {
            const paymentMethodCreditToDelete = await database.medio_pago_credito.findOne({ where: { id_medio_pago: Number(id) } });

            if (paymentMethodCreditToDelete) {
                const deletedPaymentMethodCredit = await database.medio_pago_credito.destroy({
                    where: { id_medio_pago: Number(id) }
                });
                return deletedPaymentMethodCredit;
            }
            return null;
        } catch (error) {
            throw error;
        }
    }
}

export default PaymentMethodCreditService;