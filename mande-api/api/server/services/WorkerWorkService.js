import database from '../src/models';
var Sequelize = require('sequelize');

class WorkerWorkService {
  static async getAllWorkerWorks() {
    try {
      return await database.labor_trabajador.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addWorkerWork(newWorkerWork) {
    try {
      return await database.labor_trabajador.create(newWorkerWork);
    } catch (error) {
      throw error;
    }
  }

  static async updateWorkerWorkById(id, updateWorkerWork) {
    try {
      const workerWorkToUpdate = await database.labor_trabajador.findOne({
        where: { id_labor_trabajador: Number(id) }
      });

      if (workerWorkToUpdate) {
        await database.labor_trabajador.update(updateWorkerWork, { where: { id_labor_trabajador: Number(id) } });
        return updateWorkerWork;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getWorkerWorkById(id) {
    try {
      const theWorkerWork = await database.labor_trabajador.findAll({
        include: [{
          model: await database.usuario_trabajador, required: true, where: { trabajador_ocupado: false }, as: "trabajador_labor"
        }], where: { id_labor: Number(id) }, order: Sequelize.literal('labor_precio', 'DESC')
      });
      return theWorkerWork;
    } catch (error) {
      throw error;
    }
  }

  static async getWorkerWorkReceiptById(id) {
    try {
      const theWorkerWork = await database.labor_trabajador.findAll({
        include: [{
          model: await database.recibo, required: true, where: { recibo_estado: "pendiente" }, as: "labor_trabajador_recibo_usuario"
        }], where: { id_usuario: Number(id) }
      });
      return theWorkerWork;
    } catch (error) {
      throw error;
    }
  }

  static async getWorkerWorkUserById(id) {
    try {
      const theWorkerWork = await database.labor_trabajador.findAll({
        include: [{
          model: await database.usuario_trabajador, required: true, where: { trabajador_ocupado: false }, as: "trabajador_labor",
          include: [{
            model: await database.usuario, required: true, as: "usuario_trabajador",
            include: [{
              model: await database.direccion, required: false, as: "direccion_usuario"
            }]
          }]
        }], where: { id_labor: Number(id) }, order: Sequelize.literal('labor_precio', 'DESC')
      });
      return theWorkerWork;
    } catch (error) {
      throw error;
    }
  }

  static async deleteWorkerWorkById(id) {
    try {
      console.log(id);
      const workerWorkToDelete = await database.labor_trabajador.findOne({ where: { id_labor_trabajador: Number(id) } });
      if (workerWorkToDelete) {
        const deletedWorkerWork = await database.labor_trabajador.destroy({
          where: { id_labor_trabajador: Number(id) }
        });
        return deletedWorkerWork;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default WorkerWorkService;