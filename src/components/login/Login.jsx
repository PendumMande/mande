import React from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import LoginService from '../../service/LoginService';
import Alert from '@material-ui/lab/Alert';


class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            message: null,
            errorOnLogin: false
        }
        this.login = this.login.bind(this);
    }

    componentDidMount() {
        localStorage.clear();
    }

    login = (e) => {
        e.preventDefault();
        return LoginService.doLogin(this.state.email, this.state.password)
            .then(resp => {
                if(resp == "404"){
                    this.setState({ errorOnLogin: true });
                } else {
                    localStorage.setItem("userdata", JSON.stringify(resp.data.data));
                    localStorage.setItem("logged", true);
                    this.props.history.push('/profile');
                }
            });
    }

    createAccount = (e) => {
        this.props.history.push("/register");
    }

    onChange = (e) =>
    this.setState({ [e.target.id]: e.target.value });

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Iniciar Sesion</Typography>
                <form style={formContainer}>

                    <Card className={classes.root} style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                component="img"
                                alt="Contemplative Reptile"
                                height="50%"
                                image={require('../../static/images/mande-logo.png')}
                                title="Contemplative Reptile"
                                style={{ width: '20%', alignItems: 'center', alignContent: 'center', margin: 'auto', marginTop: '2em' }}
                            />
                            <CardContent>
                                <div style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                type="email"
                                                id="email"
                                                label="Ingrese su correo"
                                                value={this.state.username}
                                                onChange={this.onChange}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                type="password"
                                                id="password"
                                                label="Ingrese su contraseña"
                                                value={this.state.password}
                                                onChange={this.onChange}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                </div>
                            </CardContent>
                        </CardActionArea>
                        <CardActions style={{ width: 'fit-content', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>

                            <Button size="small" onClick={this.login} color="primary">
                                Iniciar Sesion
        </Button>
                                <Button size="small" onClick={this.createAccount} color="primary">
                                    Crear Cuenta
        </Button>
                        </CardActions>
                         {this.state.errorOnLogin ? <Alert severity="error">Usuario no encontrado!</Alert> : null}
                    </Card>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style = {
    display: 'flex',
    justifyContent: 'center'

};

const classes = {
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    }
};

export default Login;