import React from 'react';
import './App.css';
import AppRouter from "../RouterComponent";
import NavBar from "../navbar/NavBar";
import Container from '@material-ui/core/Container';

class App extends React.Component {
  render() {
  return (
    <div>
      <NavBar />
      <Container>
        <AppRouter />
      </Container>
    </div>
  );
  }
}

export default App;
