import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from "./login/Login";
import Register from './register/Register';
import Requests from './requests/Requests';
import Profile from './profile/Profile';
import MandeMap from './map/Map';
import Request from './works/Request';
import React from "react";

class AppRouter extends React.Component {
    render() {
        return (
        <div style={style}>
            <Router>
                <Switch>
                    <Route path="/" exact component={Login} />
                    <Route path="/login" exact component={Login} />
                    <Route path="/map" exact component={MandeMap} />
                    <Route path="/requests" exact component={Requests} />
                    <Route path="/works" exact component={Request} />
                    <Route path="/register" component={Register} />
                    <Route path="/profile" component={Profile} />
                </Switch>
            </Router>
        </div>
    );
        }
}

const style = {
    marginTop: '20px'
}

export default AppRouter;