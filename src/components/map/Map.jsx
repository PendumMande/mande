import React, { Component } from 'react';
import './Map.css';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';

const columns = [
  { id: 'name', label: 'Nombre', minWidth: 170 },
  { id: 'mobile', label: 'Telefono', minWidth: 100 },
  { id: 'email', label: 'Correo', minWidth: 100 },
  { id: 'address', label: 'Direccion', minWidth: 100 },
  { id: 'action', label: 'Accion', minWidth: 100 }
];

class MandeMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      lat: '3.480593490564843',
      lon: '-76.49823380877771',
      address: 'Una Direccion',
      name: 'Cliente',
      mobile: '3508765243',
      email: 'algo@algo.com'
    }
  }

  componentDidMount() {
    if (localStorage.getItem("logged")) {
      const profileJson = JSON.parse(localStorage.getItem("userdata"));
      this.setState({ fullname: profileJson.usuario_nombre });
    } else {
      localStorage.clear();
      this.props.history.push('/');
    }
  }

  render() {
    return (
      <div style={{ width: '78%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
      <Map center={[this.state.lat, this.state.lon]} zoom={14}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={[this.state.lat, this.state.lon]} >
          <Popup>
            <div>
              <h1>Direccion: </h1>
              <p>{this.state.address}</p>
            </div>
          </Popup>
        </Marker>
      </Map>
      
      <Paper style={{width: '100%'}}>
      <TableContainer style={{maxHeight: 440}}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
                <TableRow hover role="checkbox" tabIndex={-1}>
                      <TableCell>
                        {this.state.name}
                      </TableCell>
                      <TableCell>
                        {this.state.mobile}
                      </TableCell>
                      <TableCell>
                        {this.state.email}
                      </TableCell>
                      <TableCell>
                        {this.state.address}
                      </TableCell>
                      <TableCell>
                      <Button size="small" color="primary">
                                Terminar Trabajo
        </Button>
                      </TableCell>
                </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
      </div>
    );
  }
}

export default MandeMap;