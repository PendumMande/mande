import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import SearchIcon from '@material-ui/icons/Search';
import MoreIcon from '@material-ui/icons/MoreVert';
import AddLocationIcon from '@material-ui/icons/AddLocation';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

const requests = [
  {
    id: 1,
    primary: 'David Villa',
    secondary: 'Necesito un mandado 1',
    person: '/static/images/avatar/5.jpg',
  },
  {
    id: 2,
    primary: 'Arianny Janse',
    secondary: 'Necesito un mandado 2',
    person: '/static/images/avatar/1.jpg',
  },
  {
    id: 3,
    primary: 'Angelica',
    secondary: 'Necesito un mandado 3',
    person: '/static/images/avatar/2.jpg',
  },
  {
    id: 4,
    primary: 'Duque',
    secondary: 'Necesito un mandado 4',
    person: '/static/images/avatar/3.jpg',
  },
  {
    id: 5,
    primary: "Alejandro",
    secondary: 'Necesito un mandado 5',
    person: '/static/images/avatar/4.jpg',
  },
  {
    id: 6,
    primary: 'Pepito',
    secondary: 'Necesito un mandado 5',
    person: '/static/images/avatar/5.jpg',
  },
  {
    id: 7,
    primary: 'Pepita',
    secondary: 'Necesito un mandado 6',
    person: '/static/images/avatar/1.jpg',
  },
];

class Requests extends Component {

  constructor(props) {
    super(props)
    this.state = {
      message: null
    }
  }

  componentDidMount() {
    if (localStorage.getItem("logged")) {
      const profileJson = JSON.parse(localStorage.getItem("userdata"));
      this.setState({ fullname: profileJson.usuario_nombre });
    } else {
      localStorage.clear();
      this.props.history.push('/');
    }
  }

  goToMap = (e) => {
    console.log(e);
    this.setState({ lat: '3.480593490564843', lon: '-76.49823380877771', address: 'una direccion' });
    this.props.history.push('/map');
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Paper square style={{ paddingBottom: '50' }}>
          <Typography variant="h5" gutterBottom>
            Mandados
        </Typography>
          <List style={{ marginBottom: '2' }}>
            {requests.map(({ id, primary, secondary, person }) => (
              <React.Fragment key={id}>
                <ListItem button>
                  <ListItemAvatar>
                    <Avatar alt="Profile Picture" src={person} />
                  </ListItemAvatar>
                  <ListItemText primary={primary} secondary={secondary} />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="acceptar" onClick={this.goToMap}>
                      <AddLocationIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </React.Fragment>
            ))}
          </List>
        </Paper>
        <AppBar position="fixed" color="primary" style={{ top: 'auto', bottom: 0 }}>
          <Toolbar>
            <div style={{ flexGrow: 1 }} />
            <IconButton color="inherit">
              <SearchIcon />
            </IconButton>
            <IconButton edge="end" color="inherit">
              <MoreIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    );
  }
}

export default Requests;