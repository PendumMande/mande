import React from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';


class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            address: '',
            password: '***********',
            profile_pic: '',
            enabled: false,
            message: null
        }
        this.onChange = this.onChange.bind(this);
        this.editProfile = this.editProfile.bind(this);
        this.handleGoRequest = this.handleGoRequest.bind(this);
    }

    componentDidMount() {
        if (localStorage.getItem("logged")) {
            const profileJson = JSON.parse(localStorage.getItem("userdata"));
            this.setState({ fullname: profileJson.usuario_nombre });
            this.setState({ address: profileJson.direccion_usuario.direccion_direccion });
            this.setState({ profile_pic: profileJson.usuario_foto_perfil });
        } else {
            localStorage.clear();
            this.props.history.push('/');
        }
    }

    editProfile = (e) =>
        this.state.enabled ? this.setState({ enabled: false }) : this.setState({ enabled: true });

    onChange = (e) =>
        this.setState({ [e.target.id]: e.target.value });

    handleGoRequest = (e) => this.props.history.push("/works");

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Mi Perfil</Typography>
                <form style={formContainer}>

                    <Card className={classes.root} style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                component="img"
                                alt="Contemplative Reptile"
                                height="50%"
                                image={process.env.PUBLIC_URL + '/img/profile/' + this.state.profile_pic}
                                title="Contemplative Reptile"
                                style={{ width: '20%', alignItems: 'center', alignContent: 'center', margin: 'auto', marginTop: '2em' }}
                            />
                            <CardContent>
                                <div style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                type="text"
                                                id="fullname"
                                                label="Nombre"
                                                disabled={(this.state.enabled) ? "" : "disabled"}
                                                value={this.state.fullname}
                                                onChange={this.onChange}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                type="text"
                                                id="address"
                                                label="Direccion"
                                                disabled={(this.state.enabled) ? "" : "disabled"}
                                                value={this.state.address}
                                                onChange={this.onChange}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                type="password"
                                                id="password"
                                                label="Contraseña"
                                                disabled={(this.state.enabled) ? "" : "disabled"}
                                                value={this.state.password}
                                                onChange={this.onChange}
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                </div>
                            </CardContent>
                        </CardActionArea>
                        <CardActions style={{ width: 'fit-content', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>

                            <Button size="small" onClick={this.editProfile} color="primary">
                                {(this.state.enabled) ? "Guardar Cambios" : "Editar Perfil"}
                            </Button>
                            <Button size="small" onClick={this.handleGoRequest} color="primary">
                                Solicitar Mandado
        </Button>
                        </CardActions>
                    </Card>
                </form>
            </div>
        );
    }
}

const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style = {
    display: 'flex',
    justifyContent: 'center'

};

const classes = {
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    }
};

export default Profile;