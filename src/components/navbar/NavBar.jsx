import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { Router } from "react-router-dom";
import history from "./history.js";

class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            auth: false,
            userType: 0,
            anchorEl: null,
            open: false,
            message: null,
            browserHistory: null
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleMenu = this.handleMenu.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleRequests = this.handleRequests.bind(this);
        this.handleMain = this.handleMain.bind(this);
        this.handleProfile = this.handleProfile.bind(this);
    }

    componentDidMount() {
        let utype = JSON.parse(localStorage.getItem("userdata"));
        this.setState({ userType: utype });
        this.setState({ auth: localStorage.getItem("logged") });
    }

    handleChange = (e) => {
        this.setState({ auth: !this.state.auth });
    };

    handleMenu = (e) => {
        this.setState({ anchorEl: e.currentTarget });
        this.setState({ open: true });
        console.log(this.state.anchorEl)
    };

    handleClose = (e) => {
        this.setState({ anchorEl: null });
        this.setState({ open: false });
    };

    handleRequests = (e) => {
        e.preventDefault();
        this.setState({ message: 'Logged in.' });
        history.push('/requests');
        window.location.reload(false);
    }

    handleMain = (e) => {
        e.preventDefault();
        this.setState({ message: 'Logged in.' });
        history.push('/works');
        window.location.reload(false);
    }

    handleProfile = (e) => {
        e.preventDefault();
        history.push('/profile');
        window.location.reload(false);
    }

    render() {
        return (
            <Router history={history}>
                <div style={{ flexGrow: 1 }}>
                    <AppBar position="static">
                        <Toolbar>
                            <Typography variant="h6" style={{ flexGrow: 1 }}>
                                MANDE
          </Typography>
                            {this.state.auth == true ? (<div >
                                <IconButton
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={this.handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={this.state.anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem onClick={this.handleProfile.bind(this)}>Mi Perfil</MenuItem>
                                    {this.state.userType == 1 ? <MenuItem onClick={this.handleRequests.bind(this)}>Mandados</MenuItem> : null}
                                    {this.state.userType == 2 ? <MenuItem onClick={this.handleMain.bind(this)}>Solicitar Mandado</MenuItem> : null}
                                </Menu>
                            </div>) : (<div></div>)}
                        </Toolbar>
                    </AppBar>
                </div>
            </Router>
        );
    }
}

export default NavBar;
