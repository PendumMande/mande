import React, { useState, useEffect, useLayoutEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import Payment from '../payment-method/Payment';
import MuiAlert from '@material-ui/lab/Alert';
import WorkService from "../../service/workService";
import axios from 'axios';
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  gridList: {
    width: '100%',
    height: '100%',
    transform: 'translateZ(0)',
  },
  titleBar: {
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
      'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  icon: {
    color: 'white',
  }
}));

function getSteps() {
  return ['Seleccione un tipo de trabajo', 'Seleccione un trabajador', 'Añada una descripcion', 'Metodo de pago'];
}

function getWorksOutside() {
  return axios.get('http://127.0.0.1:8084/api/v1/work/')
  .then((response) => {
    return JSON.parse(JSON.stringify(response.data.data));
  })
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Request() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const steps = getSteps();
  const [idUser, setIdUser] = React.useState(JSON.parse(localStorage.getItem("userdata")).id_usuario);
  const [idWorkWorker, setIdWorkWorker] = React.useState('');
  const [selectedWork, setSelectedWork] = React.useState('');
  const [idScore, setIdScore] = React.useState(null);
  const [idBillPayment, setIdBillPayment] = React.useState('');
  const [idBillSDate, setIdBillSDate] = React.useState(moment().format("YYYY-MM-DD hh:mm:ss"));
  const [idBillEDate, setIdBillEDate] = React.useState(moment().format("YYYY-MM-DD hh:mm:ss"));
  const worksoutside = getWorksOutside();
  const [currentWorks, setCurrentWorks] = React.useState([]);
  const [currentWorkers, setCurrentWorkers] = React.useState([]);
  const [deliverydesc, setDeliverydesc] = React.useState('');
  const [isChecked, setIsChecked] = React.useState([0]);

  const workerData = [
    {
      id_usuario: 0,
      name: 'Cargando Datos',
      trabajador_foto_cedula: '',
      id_labor_trabajador: 0,
      meters: 10
    }
  ]

  console.log(currentWorks);

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    if((activeStep + 1) === steps.length){
      axios.post('http://localhost:8084/api/v1/receipt', {
        id_usuario_cliente: parseInt(idUser),
        id_labor_trabajador: parseInt(idWorkWorker),
        recibo_puntuacion: null,
        recibo_estado: "En Progreso",
        recibo_descripcion: deliverydesc,
        recibo_pago: parseInt(idBillPayment),
        recibo_fecha_inicio: idBillSDate,
        recibo_fecha_fin: idBillEDate
    }).catch(ex => ex.response.status)
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleToggle = (value) => () => {
    setIsChecked(value);
  };

  const handleDeliveryDesc = (event) => {
    setDeliverydesc(event.target.value);
  };

  const getWorkerByWorkId = (event) => {
    setSelectedWork(event.target.name || 'Pintor');
    let labor_search = Number(event.target.id) || 1;
    axios.get('http://127.0.0.1:8084/api/v1/workerWork/' + labor_search)
      .then((response) => {
        let workerWorkJson = JSON.parse(JSON.stringify(response.data.data))[0];
        setIdWorkWorker(workerWorkJson.id_labor_trabajador);
        setIdBillPayment(workerWorkJson.labor_precio);
        return setCurrentWorkers(JSON.parse(JSON.stringify(response.data.data)));
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
      });
  };

  useLayoutEffect(() => {
    axios.get('http://127.0.0.1:8084/api/v1/work/')
      .then((response) => {
        localStorage.setItem("works", JSON.stringify(response.data.data));
        return setCurrentWorks(JSON.parse(JSON.stringify(response.data.data)));
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
      });
}, []);

  return (
    <div className={classes.root}>
      {activeStep === 0 ? (<GridList cellHeight={200} cols={3} spacing={1} className={classes.gridList} style={{ margin: 'auto' }}>
        {currentWorks.map((tile) => (
          <GridListTile key={tile.id_labor} cols={1} rows={1}>
            <img src={process.env.PUBLIC_URL + '/img/jobs/' + tile.labor_imagen} alt={tile.labor_nombre} />
            <GridListTileBar
              title={tile.labor_nombre}
              titlePosition="bottom"
              actionIcon={
                <IconButton aria-label={`star ${tile.labor_nombre}`} onClick={getWorkerByWorkId} className={classes.icon}>
                  <StarBorderIcon id={tile.id_labor} name={tile.labor_nombre} />
                </IconButton>
              }
              actionPosition="left"
              className={classes.titleBar}
            />
          </GridListTile>
        ))}
      </GridList>) : <div></div>}
      {activeStep === 1 ? (
        <List dense className={classes.root}>
          {
            currentWorkers.map((worker) => {
              const labelId = `checkbox-list-secondary-label-${worker.id_usuario}`;
              return (
                <ListItem key={worker.id_labor_trabajador} button>
                  <ListItemAvatar>
                    <Avatar
                      src={`/static/images/avatar/${worker.trabajador_labor.trabajador_foto_cedula}.png`}
                    />
                  </ListItemAvatar>
                  <ListItemText id={worker.id_usuario} primary={worker.name || "Pepito" + worker.id_usuario}
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          variant="body2"
                          className={classes.inline}
                          color="textPrimary"
                        >
                          Este trabajador se encuentra en {worker.meters || "Carrera 76 # 16 - 41"}
                        </Typography>
                      </React.Fragment>
                    }
                  />
                  <ListItemSecondaryAction>
                    <Checkbox
                      edge="end"
                      onChange={handleToggle(worker.id_usuario)}
                      checked={isChecked === worker.id_usuario}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}
        </List>
      ) : <div></div>}
      {activeStep === 2 ? (<Container fixed>
        <FormControl fullWidth className={classes.margin}>
          <InputLabel htmlFor="standard-adornment-amount">Por favor ingrese la descripcion del trabajo solicitado</InputLabel>
          <Input
            id="standard-adornment-amount"
            value={deliverydesc}
            onChange={handleDeliveryDesc}
          />
        </FormControl>
      </Container>) : <div></div>}
      {activeStep === 3 ? (
        <Container fixed>
          <FormControl fullWidth className={classes.margin}>
            <InputLabel htmlFor="standard-adornment-amount">Valor a pagar</InputLabel>
            <Input
              id="standard-adornment-amount"
              value="10000"
              disabled
              startAdornment={<InputAdornment position="start">$</InputAdornment>}
            />
          </FormControl>
          <Typography variant="h6" component="h6">Seleccione un metodo de pago:</Typography>

          <Payment />

        </Container>
      ) : <div></div>}
      {activeStep === 4 ? (
        <Container fixed>
          <Alert severity="success">La solictud ha sido aprobada exitosamente!</Alert>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography className={classes.heading}>Trabajo</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                Se ha solicitado un {selectedWork} 
          </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={classes.heading}>Descripcion</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                {deliverydesc}
          </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography className={classes.heading}>Metodo de pago</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                Pago {idBillPayment} en debito
          </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Container>
      ) : <div></div>}
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Button onClick={handleReset} className={classes.button}>
              Inicio
            </Button>
          </div>
        ) : (
            <div>
              <div>
                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                  Atras
              </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className={classes.button}
                >
                  {activeStep === steps.length - 1 ? 'Finalizar' : 'Siguiente'}
                </Button>
              </div>
            </div>
          )}
      </div>
    </div>
  );
}