import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { green } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import 'date-fns';

function Payment(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`action-tabpanel-${index}`}
      aria-labelledby={`action-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

Payment.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `action-tab-${index}`,
    'aria-controls': `action-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '74em',
    position: 'relative',
    minHeight: 200,
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fabGreen: {
    color: theme.palette.common.white,
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  creditRoot: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
}));

export default function FloatingActionButtonZoom() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="action tabs example"
        >
          <Tab label="Debito" {...a11yProps(0)} />
          <Tab label="Credito" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <Payment value={value} index={0} dir={theme.direction}>   
        <TextField
          id="outlined-full-width"
          label="Numero de cuenta"
          style={{ margin: 8 }}
          placeholder="Por favor ingrese su numero de cuenta"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        />
        </Payment>
        <Payment value={value} index={1} dir={theme.direction}>
        <form style={{ width: '100%', alignItems: 'center', alignContent: 'center', margin: 'auto' }} className={classes.creditRoot} noValidate autoComplete="off">
        <TextField style={{width: '40%'}} id="filled-basic" margin='dense' label="Numero de tarjeta" variant="filled" />
  
        <TextField style={{width: '40%'}} id="filled-basic" margin='dense' label="Nombre" helperText="Como aparece en la tarjeta" variant="filled" />
</form>
<form style={{ width: '100%', alignItems: 'center', alignContent: 'center', margin: 'auto' }} className={classes.creditRoot} noValidate autoComplete="off">
<TextField style={{width: '25%'}} id="filled-basic" margin='dense' label="Mes expiracion" variant="filled" />
<TextField style={{width: '25%'}} id="filled-basic" margin='dense' label="Año Expiracion" variant="filled" />
<TextField style={{width: '25%'}} id="filled-basic" type='password' margin='dense' label="CVV" helperText="Numero de atras de la tarjeta" variant="filled" />
</form>
        </Payment>
      </SwipeableViews>
    </div>
  );
}