import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';


class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            fullname: '',
            address: '',
            password: '',
            message: null
        }
        this.saveUser = this.saveUser.bind(this);
    }

    login = (e) => {
        this.props.history.push("/login");
    }

    onChange = (e) =>
        this.setState({ [e.target.id]: e.target.value });

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Crear Cuenta</Typography>
                <form style={formContainer}>

                    <Card style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="50%"
                                image={require('../../static/images/mande-logo.png')}
                                title="Contemplative Reptile"
                                style={{ width: '20%', alignItems: 'center', alignContent: 'center', margin: 'auto', marginTop: '2em' }}
                            />
                            <CardContent>
                                <div style={{ width: '50%', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                id="email"
                                                type="email"
                                                value={this.state.email}
                                                onChange={this.onChange}
                                                label="Ingrese su correo"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                id="fullname"
                                                type="text"
                                                value={this.state.fullname}
                                                onChange={this.onChange}
                                                label="Ingrese su nombre completo"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                id="address"
                                                type="text"
                                                value={this.state.address}
                                                onChange={this.onChange}
                                                label="Ingrese su direccion"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={1} alignItems="flex-end">
                                        <Grid item>
                                            <AccountCircle />
                                        </Grid>
                                        <Grid item>
                                            <TextField margin="normal"
                                                fullWidth
                                                style={{ margin: 8 }}
                                                id="password"
                                                type="password"
                                                value={this.state.password}
                                                onChange={this.onChange}
                                                label="Ingrese su contraseña"
                                                InputLabelProps={{
                                                    shrink: true,
                                                }} />

                                        </Grid>
                                    </Grid>
                                </div>
                            </CardContent>
                        </CardActionArea>
                        <CardActions style={{ width: 'fit-content', alignItems: 'center', alignContent: 'center', margin: 'auto' }}>
                            <Button size="small" onClick={this.saveUser} color="primary">
                                Crear Cuenta
        </Button>
                            <Button size="small" onClick={this.login} color="primary">
                                Iniciar Sesion
        </Button>
                        </CardActions>
                    </Card>
                </form>
            </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style = {
    display: 'flex',
    justifyContent: 'center'

};

export default Register;