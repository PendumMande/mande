import axios from 'axios';

const WORK_API_BASE_URL = 'http://127.0.0.1:8084/api/v1/work/';

class WorkService {

    getWorks() {
        return axios.get(WORK_API_BASE_URL).then(response => response.data.data);
    }

}

export default new WorkService();
