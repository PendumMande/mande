import axios from 'axios';

const LOGIN_API_BASE_URL = 'http://localhost:8084/api/v1/user/login/';

class LoginService {

    doLogin(email_mobile_user, userPassword) {
        if (/^\d+$/.test(email_mobile_user)) {
            return axios.post(LOGIN_API_BASE_URL, {
                email_mobile: parseInt(email_mobile_user),
                usuario_contrasena: userPassword
            }).catch(ex => ex.response.status)
        } else {
            return axios.post(LOGIN_API_BASE_URL, {
                email_mobile: email_mobile_user,
                usuario_contrasena: userPassword
            }).catch(ex => ex.response.status)
        }
    }

}

export default new LoginService();
